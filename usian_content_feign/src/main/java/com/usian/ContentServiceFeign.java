package com.usian;

import com.usian.utils.AdNode;
import com.usian.utils.PageResult;
import com.usian.pojo.TbContent;
import com.usian.pojo.TbContentCategory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "usian-content-service")
public interface ContentServiceFeign {

    @RequestMapping("/service/contentCategory/selectContentCategoryByParentId")
    List<TbContentCategory> selectContentCategoryByParentId(@RequestParam Long id);

    @PostMapping("/service/contentCategory/insertContentCategory")
    Integer insertContentCategory(@RequestBody TbContentCategory tbContentCategory);
    @RequestMapping("/service/contentCategory/deleteContentCategoryById")
    Integer deleteContentCategoryById(@RequestParam Long categoryId);
    @PostMapping("/service/contentCategory/updateContentCategory")
    Integer updateContentCategory(@RequestBody TbContentCategory tbContentCategory);
    @PostMapping("/service/contentCategory/selectTbContentAllByCategoryId")
    PageResult selectTbContentAllByCategoryId(@RequestParam("page") Integer page,
                                              @RequestParam("rows") Integer rows, @RequestParam("categoryId") Long categoryId);
    @PostMapping("/service/contentCategory/insertTbContent")
    Integer insertTbContent(@RequestBody TbContent tbContent);

    @RequestMapping("/service/contentCategory/deleteContentByIds")
    Integer deleteContentByIds(@RequestParam("id") Long id);
    @GetMapping("/service/contentCategory/selectFrontendContentByAD")
    List<AdNode> selectFrontendContentByAD();
}
