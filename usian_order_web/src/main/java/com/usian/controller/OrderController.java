package com.usian.controller;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.usian.feign.ItemServiceFeign;
import com.usian.feign.OrderFeign;
import com.usian.pojo.OrderInfo;
import com.usian.pojo.TbItem;
import com.usian.pojo.TbOrder;
import com.usian.pojo.TbOrderShipping;
import com.usian.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 订单服务 Controller
 */
@RestController
@RequestMapping("/frontend/order")
public class OrderController {
    @Autowired
    private ItemServiceFeign cartServiceFeign;

    @Autowired
    private OrderFeign orderServiceFeign;
    @RequestMapping("/goSettlement")
    public Result goSettlement(String[] ids, String userId) {
         //获取购物车
        Map<String, TbItem> cart = cartServiceFeign.selectCartByUserId(userId);
      //从购物车中获取选中的商品
        ArrayList<TbItem> tbItems = new ArrayList<>();
        for (String id : ids) {
            tbItems.add(cart.get(id));
        }
         if (tbItems.size()>0){
             return Result.ok(tbItems);
         }
         return Result.error("error");


    }
    /**
     * 创建订单
     */
    @RequestMapping("/insertOrder")
    public Result insertOrder(@RequestParam String orderItem, TbOrder tbOrder , TbOrderShipping tbOrderShipping) {
        //因为一个request中只包含一个request body. 所以feign不支持多个@RequestBody。
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderItem(orderItem);
        orderInfo.setTbOrder(tbOrder);
        orderInfo.setTbOrderShipping(tbOrderShipping);
        Long orderid= orderServiceFeign.insertOrder(orderInfo);
         if (orderid!=null){
             return Result.ok("ok");
         }
        return Result.error("error");
    }

}
