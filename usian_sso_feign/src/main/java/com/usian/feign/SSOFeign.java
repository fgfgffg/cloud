package com.usian.feign;

import com.usian.pojo.TbUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(value = "usian-sso-service")
public interface SSOFeign {
    @RequestMapping("/service/checkUserInfo/{checkValue}/{checkFlag}")
    public boolean checkUserInfo(@PathVariable String checkValue,
                                 @PathVariable Integer checkFlag);
    @RequestMapping("/service/userRegister")
    public Integer userRegister(@RequestBody TbUser user);
    @RequestMapping("/service/userLogin")
    public Map<String,String> userLogin(@RequestParam String username, @RequestParam String password);
    @PostMapping("/service/getUserByToken/{token}")
    TbUser getUserByToken(String token);
    @PostMapping("/service/logOut")
    public Boolean logOut(@RequestParam String token);
}
