package com.usian.utils;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;
@ApiModel
public class PageResult implements Serializable {
    private Integer pageIndex; //当前页
    private Integer totalPage; //总页数
    private List result; //结果集

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List getResult() {
        return result;
    }

    public void setResult(List result) {
        this.result = result;
    }
}
