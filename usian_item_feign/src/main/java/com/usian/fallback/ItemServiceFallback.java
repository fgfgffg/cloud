package com.usian.fallback;

import com.usian.feigin.FeignInter;
import com.usian.pojo.*;
import com.usian.utils.CatResult;
import com.usian.utils.PageResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * item 服务返回托底数据,降级返回数据
 */
@Component
public class ItemServiceFallback implements FallbackFactory<FeignInter> {

    @Override
    public FeignInter create(Throwable throwable) {
        return null;
    }

    public TbItem selectItemInfo(Long itemId) {
        return null;
    }


    public PageResult selectTbItemAllByPage(Integer page, Integer rows) {
        return null;
    }


    public List<TbItemCat> selectItemCategoryByParentId(Long id) {
        return null;
    }
    public TbItemParam selectItemParamByItemCatId(Long itemCatId) {
        return null;
    }

    public Integer insertTbItem(TbItem tbItem, String desc, String itemParams) {
        return null;
    }

    public PageResult selectItemParamAll(Integer page, Integer rows) {
        return null;
    }

    public Integer insertItemParam(TbItemParam tbItemParam) {
        return null;
    }


    public CatResult selectItemCategoryAll() {
        return null;
    }

    public TbItemDesc selectItemDescByItemId(Long itemId) {
        return null;
    }

    public TbItemParamItem selectTbItemParamItemByItemId(Long itemId) {
        return null;
    }


}
