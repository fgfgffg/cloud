package com.usian.feigin;
import com.usian.fallback.ItemServiceFallback;
import com.usian.pojo.*;
import com.usian.utils.CatResult;
import com.usian.utils.PageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@FeignClient(value = "usian-item-service",fallbackFactory = ItemServiceFallback.class)
public interface FeignInter {

    @RequestMapping("service/selectTbItemAllByPage")
   PageResult selectTbItemAllByPage(@RequestParam Integer page);
    @RequestMapping("service/selectItemCategoryByParentId")
    public  List<TbItemCat> selectItemCategoryByParentId(@RequestParam Long id );
    @RequestMapping("service/selectItemParamByItemCatId/{itemCatId}")
    public  TbItemParam selectItemParamByItemCatId(@PathVariable(value="itemCatId") Long itemCatId);

    @RequestMapping("service/insertTbItem")
    public Integer insertTbItem(@RequestBody TbItem tbItem, @RequestParam String desc,
                                @RequestParam String itemParams);
    @RequestMapping("service/deleteItemById")
     Integer deleteItemById(@RequestParam("itemId") Long itemId);
    @RequestMapping("service/preUpdateItem")
    public Map<String,Object> preUpdateItem(@RequestParam Long itemId);
    @RequestMapping("service/updateTbItem")
    public  Integer updateTbItem(@RequestBody TbItem tbItem,@RequestParam String desc,@RequestParam String itemParams);
    @RequestMapping("service/selectItemParamAll")
    PageResult selectItemParamAll(@RequestParam Integer page,
                                  @RequestParam Integer rows);
    @RequestMapping("service/insertItemParam")
    Integer insertItemParam(@RequestParam Long itemCatId,
                            @RequestParam String paramData);
    @RequestMapping("service/deleteItemParamById")
    Integer deleteItemParamById(@RequestParam Long id);

    @RequestMapping(value = "service/selectItemCategoryAll",method = RequestMethod.GET)
    CatResult selectItemCategoryAll();
    @RequestMapping("service/selectTbItemParamItemByItemId")
    TbItemParamItem selectTbItemParamItemByItemId(@RequestParam  Long itemId);
    @RequestMapping("service/selectItemDescByItemId")

    TbItemDesc selectItemDescByItemId(@RequestParam Long itemId);
    @RequestMapping("service/selectItemInfo")
    TbItem selectItemInfo(@RequestParam  Long itemId);
}
