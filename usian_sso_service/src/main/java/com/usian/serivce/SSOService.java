package com.usian.serivce;

import com.usian.client.RedisClient;
import com.usian.mapper.TbUserMapper;
import com.usian.pojo.TbUser;
import com.usian.pojo.TbUserExample;
import com.usian.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class SSOService {
    @Autowired
    private TbUserMapper tbUserMapper;
    public boolean checkUserInfo(String checkValue, Integer checkFlag) {
        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
        // 1、查询条件根据参数动态生成：1、2分别代表username、phone
        if (checkFlag==1){
            criteria.andUsernameEqualTo(checkValue);
        }else if (checkFlag==2){
            criteria.andPhoneEqualTo(checkValue);
        }
        // 2、从tb_user表中查询数据
        List<TbUser> tbUsers = tbUserMapper.selectByExample(tbUserExample);
        // 3、判断查询结果，如果查询到数据返回false。
        if (tbUsers==null || tbUsers.size()==0){
            // 4、如果没有返回true。
            return true;
        }
        // 5、如果有返回false。
          return false;
    }
    @Autowired
    private RedisClient redisClient;
    @Value("USER_INFO")
  private   String USER_INFO;

    public Map userLogin(String username, String password) {
         //判断用户名,密码是否正确
        String digest = MD5Utils.digest(password);
        TbUserExample tbUserExample = new TbUserExample();
        TbUserExample.Criteria criteria = tbUserExample.createCriteria();
   criteria.andUsernameEqualTo(username);
   criteria.andPasswordEqualTo(digest);
        List<TbUser> tbUsers = tbUserMapper.selectByExample(tbUserExample);
       if (tbUsers==null || tbUsers.size()<=0){

           return null;
       }
        TbUser tbUser = tbUsers.get(0);
       //登录成功生成token,
         String token= UUID.randomUUID().toString();
        //把用户信息保存到redis,key就是token,value就是Tbuser对象转换成的json
        tbUser.setPassword(null);
        redisClient.set(USER_INFO + ":" + token, tbUser);
        redisClient.expire(USER_INFO + ":" + token, 86400l);
        Map<String, String> map = new HashMap<>();
      map.put("token",token);
        map.put("userid",tbUser.getId().toString());
        map.put("username",tbUser.getUsername());
        return map;
    }

    public Integer userRegister(TbUser user) {
        String digest = MD5Utils.digest(user.getPassword());
         user.setPassword(digest);
          user.setCreated(new Date());
       user.setUpdated(new Date());
       return tbUserMapper.insert(user);
    }

    public TbUser getUserByToken(String token) {
        TbUser o = (TbUser) redisClient.get(USER_INFO + ":" + token);
          if (o!=null){
              redisClient.set(USER_INFO + ":" + token, o);
              redisClient.expire(USER_INFO + ":" + token,86400L);
           return o;
          }
         return null;
    }

    public Boolean logOut(String token) {
        return redisClient.del(USER_INFO + ":" + token);
    }
}
