package com.usian.service;

import com.usian.client.RedisClient;
import com.usian.mapper.*;
import com.usian.mq.MQSender;
import com.usian.pojo.*;
import com.usian.utils.JsonUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class OrderService {

    @Value("${ORDER_ID_KEY}")
    private String ORDER_ID_KEY;

    @Value("${ORDER_ID_BEGIN}")
    private Long ORDER_ID_BEGIN;

    @Value("${ORDER_ITEM_ID_KEY}")
    private String ORDER_ITEM_ID_KEY;
   @Autowired
   private RedisClient redisClient;
   @Autowired
   private TbOrderMapper tbOrderMapper;
   @Autowired
   private AmqpTemplate amqpTemplate;
@Autowired
private LocalMessageMapper localMessageMapper;
    /**
     @Autowired
     private AmqpTemplate amqpTemplate;
     */
    @Autowired
    private MQSender mqSender;
   @Autowired
   private TbOrderShippingMapper tbOrderShippingMapper;


    public Long insertOrder(OrderInfo orderInfo) {
        //1、解析orderInfo
        TbOrder tbOrder = orderInfo.getTbOrder();
        TbOrderShipping tbOrderShipping = orderInfo.getTbOrderShipping();
        List<TbOrderItem> tbOrderItems = JsonUtils.jsonToList(orderInfo.getOrderItem(), TbOrderItem.class);

        //2、保存订单信息
        if (!redisClient.exists(ORDER_ID_KEY)){
               redisClient.set(ORDER_ID_KEY,ORDER_ID_BEGIN);
        }
        long incr = redisClient.incr(ORDER_ID_KEY, 1L);
         tbOrder.setOrderId(incr+"");
        Date date = new Date();
        tbOrder.setCreateTime(date);
        tbOrder.setUpdateTime(date);
        //1、未付款，2、已付款，3、未发货，4、已发货，5、交易成功，6、交易关闭
        tbOrder.setStatus(1);
      tbOrderMapper.insert(tbOrder);
        //3、保存明细信息
          if (!redisClient.exists(ORDER_ITEM_ID_KEY)){
              redisClient.set(ORDER_ITEM_ID_KEY,0);
          }
        for (TbOrderItem tbOrderItem : tbOrderItems) {
            long orderItemId = redisClient.incr(ORDER_ITEM_ID_KEY, 1L);
            tbOrderItem.setId(incr+"");
            tbOrderItem.setOrderId(orderItemId+"");
        }
        //4、保存物流信息

          tbOrderShipping.setOrderId(incr+"");
       tbOrderShipping.setCreated(date);
        tbOrderShipping.setUpdated(date);

        //保存本地消息记录
        LocalMessage localMessage = new LocalMessage();
            localMessage.setTxNo(UUID.randomUUID().toString());
           localMessage.setOrderNo(incr+"");
        localMessage.setState(0);
          localMessageMapper.insertSelective(localMessage);
          mqSender.sendMsg(localMessage);
        //发送消息到mq,完成扣减库存
     //   amqpTemplate.convertAndSend("order_exchange","order.add",incr);
         //返回订单id
        return incr;
    }

    /**
     * 关闭超时订单
     * @param tbOrder
     */
    public void updateOverTimeTbOrder(TbOrder tbOrder) {
        tbOrder.setStatus(6);
        Date date = new Date();
        tbOrder.setUpdateTime(date);
        tbOrder.setCreateTime(date);
        tbOrder.setCloseTime(date);
        tbOrderMapper.updateByPrimaryKeySelective(tbOrder);
    }
   @Autowired
    /**
     * 查询超时订单
     * @return
     */
     public List<TbOrder> selectOvertimeOrder() {
   return tbOrderMapper.selectOvertimeOrder();
    }
  @Autowired
  private TbOrderItemMapper tbOrderItemMapper;
     @Autowired
     private TbItemMapper tbItemMapper;
    /**
     * 把订单中商品的库存数量加回去
     * @param orderId
     */
    public void updateTbItemByOrderId(String orderId) {
        //1、通过orderId查询LisT<TbOrderItem>
        TbOrderItemExample tbOrderItemExample = new TbOrderItemExample();
        TbOrderItemExample.Criteria criteria = tbOrderItemExample.createCriteria();
          criteria.andOrderIdEqualTo(orderId);
        List<TbOrderItem> tbOrderItems = tbOrderItemMapper.selectByExample(tbOrderItemExample);
        for (TbOrderItem tbOrderItem : tbOrderItems) {
            //修改商品库存
            TbItem tbItem = tbItemMapper.selectByPrimaryKey(Long.valueOf(tbOrderItem.getId()));
            tbOrderItem.setNum(tbOrderItem.getNum()+tbItem.getNum());
            tbItem.setUpdated(new Date());
            tbItemMapper.updateByPrimaryKey(tbItem);
        }



    }
}
