package com.usian.feign;

import com.usian.pojo.TbItem;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(value = "usian-cart-service")
public interface ItemServiceFeign {
    @RequestMapping("cart/selectItemInfo")
     TbItem selectItemInfo(@RequestParam  Long itemId);
    @RequestMapping("cart/selectCartByUserId")
    Map<String, TbItem> selectCartByUserId(@RequestParam String userId);

    @RequestMapping("cart/insertCart")
    Boolean insertCart(@RequestParam String userId,Map<String, TbItem> cart);

}
