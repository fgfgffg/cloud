package com.usian.service;

import com.usian.pojo.TbItem;
import com.usian.pojo.TbItemDesc;
import com.usian.pojo.TbItemParamItem;
import com.usian.utils.PageResult;
import com.usian.pojo.TbItemParam;

public interface ItemParamService {
    public TbItemParam selectItemParamByItemCatId(Long itemCatId);

    PageResult selectItemParamAll(Integer page, Integer rows);

    Integer insertItemParam(Long itemCatId, String paramData);

    Integer deleteItemParamById(Long id);

    TbItemParamItem selectTbItemParamItemByItemId(Long itemId);

    TbItem selectItemInfo(Long itemId);

    TbItemDesc selectItemDescByItemId(Long itemId);
}
