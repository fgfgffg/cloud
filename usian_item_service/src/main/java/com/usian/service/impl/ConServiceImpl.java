package com.usian.service.impl;

import com.usian.service.ConService;
import com.usian.utils.IDUtils;
import com.usian.utils.PageResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.usian.mapper.*;
import com.usian.pojo.*;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ConServiceImpl implements ConService {

    @Autowired
    private TbItemMapper tbItemMapper;
    @Autowired
    private TbItemCatMapper tbItemCatMapper;
    @Autowired
    private TbItemDescMapper tbItemDescMapper;
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private TbOrderItemMapper tbOrderItemMapper;
    @Override
    public PageResult selectTbItemAllByPage(Integer page, Integer rows) {
        PageHelper.startPage(page, rows);
        TbItemExample example = new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo((byte) 1);
//
        example.setOrderByClause("created DESC");

//          criteria.andTitleLike();
        List<TbItem> items = tbItemMapper.selectByExample(example);
        PageInfo<TbItem> info = new PageInfo<>(items);
        PageResult result = new PageResult();
        result.setPageIndex(info.getPageNum());
        result.setTotalPage(Integer.valueOf(info.getPages()));
        result.setResult(info.getList());
        return result;
    }

//    @Override
//    public Integer insertTbItem(TbItem tbItem, String desc, String itemParams) {
//       //补齐商品表的内容
//
//        Long itemId = IDUtils.genItemId();
//        Date date = new Date();
//         tbItem.setId(itemId);
//         tbItem.setStatus((byte)1);
//          tbItem.setUpdated(date);
//          tbItem.setCreated(date);
//           tbItem.setPrice(tbItem.getPrice()*100);
//        int i2 = tbItemMapper.insertSelective(tbItem);
//
//        //补齐商品描述
//        TbItemDesc tbItemDesc = new TbItemDesc();
//        tbItemDesc.setItemId(itemId);
//        tbItemDesc.setItemDesc(desc);
//        tbItemDesc.setCreated(date);
//        tbItemDesc.setUpdated(date);
//        int i1 = tbItemDescMapper.insertSelective(tbItemDesc);
//        //补齐商品规格参数
//        TbItemParamItem tbItemParamItem = new TbItemParamItem();
//         tbItemParamItem.setId(itemId);
//         tbItemParamItem.setParamData(itemParams);
//         tbItemParamItem.setUpdated(date);
//         tbItemParamItem.setCreated(date);
//        int i = tbItemParamItemMapper.insertSelective(tbItemParamItem);
//
//        return  i+i1+i2;
//    }

    @Override
    public Integer insertTbItem(TbItem tbItem, String desc, String itemParams) {
        //补齐 Tbitem 数据
        Long itemId = IDUtils.genItemId();
        Date date = new Date();
        tbItem.setId(itemId);
        tbItem.setStatus((byte)1);
        tbItem.setUpdated(date);
        tbItem.setCreated(date);
        tbItem.setPrice(tbItem.getPrice()*100);
        Integer tbItemNum = tbItemMapper.insertSelective(tbItem);

        //补齐商品描述对象
        TbItemDesc tbItemDesc = new TbItemDesc();
        tbItemDesc.setItemId(itemId);
        tbItemDesc.setItemDesc(desc);
        tbItemDesc.setCreated(date);
        tbItemDesc.setUpdated(date);
        Integer tbitemDescNum = tbItemDescMapper.insertSelective(tbItemDesc);

        //补齐商品规格参数
        TbItemParamItem tbItemParamItem = new TbItemParamItem();
        tbItemParamItem.setItemId(itemId);
        tbItemParamItem.setParamData(itemParams);
        tbItemParamItem.setUpdated(date);
        tbItemParamItem.setCreated(date);
        Integer itemParamItmeNum =
                tbItemParamItemMapper.insertSelective(tbItemParamItem);
//添加商品发布消息到mq
        amqpTemplate.convertAndSend("item_exchage","item.add", itemId);

        return tbItemNum + tbitemDescNum + itemParamItmeNum;
}

    @Override
    public Integer deleteItemById(Long itemId) {
        TbItemParamItemExample paramItemExample = new TbItemParamItemExample();
        TbItemParamItemExample.Criteria criteria = paramItemExample.createCriteria();
        criteria.andItemIdEqualTo(itemId);

        int i1 = tbItemParamItemMapper.deleteByExample(paramItemExample);
        int i = tbItemDescMapper.deleteByPrimaryKey(itemId);

        TbItem item = new TbItem();
        item.setId(itemId);
        item.setStatus((byte)3);
        int i2 = tbItemMapper.updateByPrimaryKeySelective(item);
        return i1+i2+i;
    }

    @Override
    public Map<String,Object> preUpdateItem(Long itemId) {
        Map<String, Object> map = new HashMap<>();
        TbItem item = tbItemMapper.selectByPrimaryKey(itemId);
            item.setPrice(item.getPrice()/100);
         map.put("item",item);
        TbItemDesc tbItemDesc = tbItemDescMapper.selectByPrimaryKey(itemId);

        if (tbItemDesc!=null){

        map.put("itemDesc",tbItemDesc.getItemDesc());
        }else {
        map.put("itemDesc","");

    }


        TbItemCat tbItemCat = tbItemCatMapper.selectByPrimaryKey(item.getCid());

            map.put("itemCat",tbItemCat.getName());

        TbItemParamItemExample example = new TbItemParamItemExample();
        TbItemParamItemExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(itemId);
        List<TbItemParamItem> list = tbItemParamItemMapper.selectByExampleWithBLOBs(example);
        if (list != null && list.size() > 0) {
            map.put("itemParamItem", list.get(0).getParamData());
        }
        return map;
    }

    @Override
    public Integer updateTbItem(TbItem tbItem, String desc, String itemParams) {

        Date date = new Date();

        tbItem.setStatus((byte)1);
        tbItem.setUpdated(date);

        tbItem.setPrice(tbItem.getPrice()*100);
        int i2 = tbItemMapper.updateByPrimaryKeySelective(tbItem);

        //补齐商品描述
        TbItemDesc tbItemDesc = new TbItemDesc();
        tbItemDesc.setItemId(tbItem.getId());
        tbItemDesc.setItemDesc(desc);
        tbItemDesc.setUpdated(date);
        int i1 = tbItemDescMapper.updateByPrimaryKeyWithBLOBs(tbItemDesc);
        //补齐商品规格参数
        TbItemParamItem tbItemParamItem = new TbItemParamItem();
        tbItemParamItem.setId(tbItem.getId());
        tbItemParamItem.setParamData(itemParams);
        tbItemParamItem.setUpdated(date);
        TbItemParamItemExample paramItemExample = new TbItemParamItemExample();
        TbItemParamItemExample.Criteria criteria = paramItemExample.createCriteria();
           criteria.andItemIdEqualTo(tbItem.getId());

        int i = tbItemParamItemMapper.updateByExampleSelective(tbItemParamItem,paramItemExample);

        return  i+i1+i2;
    }
    /**
     * 修改商品库存数量
     * @param orderId
     * @return
     */
    public Integer updateTbItemByOrderId(String orderId) {
        TbOrderItemExample tbOrderItemExample = new TbOrderItemExample();
        TbOrderItemExample.Criteria criteria = tbOrderItemExample.createCriteria();
        criteria.andOrderIdEqualTo(orderId);
        List<TbOrderItem> itemList = tbOrderItemMapper.selectByExample(tbOrderItemExample);
        for (TbOrderItem tbOrderItem : itemList) {
            TbItem tbItem = tbItemMapper.selectByPrimaryKey(Long.valueOf(tbOrderItem.getId()));
            if (tbItem.getNum()>0 && tbItem.getNum()>tbOrderItem.getNum()){
                tbItem.setNum(tbItem.getNum()-tbOrderItem.getNum());

            }else {
                return 0;
            }

        }
         return 1;
    }
}
