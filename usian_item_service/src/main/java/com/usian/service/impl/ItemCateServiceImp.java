package com.usian.service.impl;

import com.usian.client.RedisClient;
import com.usian.service.ItemCateService;

import com.usian.mapper.TbItemCatMapper;
import com.usian.pojo.TbItemCat;
import com.usian.pojo.TbItemCatExample;
import com.usian.utils.CatResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public class ItemCateServiceImp implements ItemCateService {
@Autowired
private TbItemCatMapper tbItemCatMapper;

    @Value("${PROTAL_CATRESULT_KEY}")
    private String PROTAL_CATRESULT_KEY;

   @Autowired
   private RedisClient redis;


    @Override
    public List<TbItemCat> selectItemCategoryByParentId(Long id) {
        TbItemCatExample catExample = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = catExample.createCriteria();
         criteria.andParentIdEqualTo(id);
         criteria.andStatusEqualTo(1);

        List<TbItemCat> cats = tbItemCatMapper.selectByExample(catExample);
        return cats;
    }

    @Override
    public CatResult selectItemCategoryAll() {
        //查询缓存
        com.usian.utils.CatResult catResultRedis = (CatResult)redis.get(PROTAL_CATRESULT_KEY);
        if(catResultRedis!=null){
            return catResultRedis;
        }

        CatResult catResult = new CatResult();
        //查询商品分类
        catResult.setData(getCatList(0L));

        //添加到缓存
        redis.set(PROTAL_CATRESULT_KEY,catResult);

        return catResult;

    }

    /**
     * 私有方法，查询商品分类
     */
    private List<?> getCatList(Long parentId){
        //创建查询条件
        TbItemCatExample example = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        List<TbItemCat> list = this.tbItemCatMapper.selectByExample(example);
        List resultList = new ArrayList();
        int count = 0;
        for(TbItemCat tbItemCat:list){
            //判断是否是父节点
            if(tbItemCat.getIsParent()){
                com.usian.utils.CatNode catNode = new com.usian.utils.CatNode();
                catNode.setName(tbItemCat.getName());
                catNode.setItem(getCatList(tbItemCat.getId()));
                resultList.add(catNode);
                count++;
                //只取商品分类中的 18 条数据
                if (count == 18){
                    break;
                }
            }else{
                resultList.add(tbItemCat.getName());
            }
        }
        return resultList;
    }
}
