package com.usian.service;

import com.usian.utils.PageResult;
import com.usian.pojo.TbItem;

import java.util.Map;

public interface ConService {


    public PageResult selectTbItemAllByPage(Integer page, Integer rows);

    public Integer insertTbItem(TbItem tbItem, String desc, String itemParams);

    public Integer deleteItemById(Long itemId);

    public Map<String,Object> preUpdateItem(Long itemId);

    public Integer updateTbItem(TbItem tbItem, String desc, String itemParams);

}
