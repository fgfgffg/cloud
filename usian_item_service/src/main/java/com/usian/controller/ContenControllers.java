package com.usian.controller;

import com.usian.service.ConService;

import com.usian.service.ItemCateService;
import com.usian.service.ItemParamService;
import com.usian.pojo.*;
import com.usian.utils.CatResult;
import com.usian.utils.PageResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/service")
public class ContenControllers {
    @Autowired
    private ConService conService;
    @Autowired
    private ItemParamService itemParamService;
    @Autowired
    private ItemCateService itemCateService;


    @RequestMapping("/selectTbItemAllByPage")
    public PageResult selectTbItemAllByPage(@RequestParam Integer page){
          return conService.selectTbItemAllByPage(page,10);

    }
    @RequestMapping("/insertTbItem")
    public Integer insertTbItem(@RequestBody TbItem tbItem,@RequestParam String desc,@RequestParam String itemParams){
        return conService.insertTbItem(tbItem,desc,itemParams);
    }
    @RequestMapping("/deleteItemById")
    public Integer deleteItemById(@RequestParam  Long itemId){
        return conService.deleteItemById(itemId);
    }
    @RequestMapping("/preUpdateItem")
    public Map<String,Object> preUpdateItem(@RequestParam Long itemId){
        return conService.preUpdateItem(itemId);
    }
    @RequestMapping("/updateTbItem")
    public  Integer updateTbItem(@RequestBody TbItem tbItem,@RequestParam String desc,@RequestParam String itemParams){
        return conService.updateTbItem(tbItem,desc,itemParams);
    }

    @RequestMapping("/selectItemCategoryByParentId")
    public List<TbItemCat> selectItemCategoryByParentId(@RequestParam Long id){
        return itemCateService.selectItemCategoryByParentId(id);
    }

    @RequestMapping("/selectItemParamByItemCatId/{itemCatId}")
    public TbItemParam selectItemParamByItemCatId(@PathVariable("itemCatId") Long itemCatId){
        return  itemParamService.selectItemParamByItemCatId(itemCatId);
    }

    @RequestMapping("/selectItemParamAll")
    public PageResult selectItemParamAll(Integer page, Integer rows){
        return  itemParamService.selectItemParamAll(page,rows);
    }
    @RequestMapping("/insertItemParam")
    public Integer insertItemParam(Long itemCatId, String paramData){
        return itemParamService.insertItemParam(itemCatId,paramData);
    }

    @RequestMapping("/deleteItemParamById")
    Integer deleteItemParamById(@RequestParam Long id){
        return itemParamService.deleteItemParamById(id);
    }
    /**
     * 查询首页商品分类
     */
    @RequestMapping("/selectItemCategoryAll")
    public CatResult selectItemCategoryAll(){
        return this.itemCateService.selectItemCategoryAll();
    }
    /**
     * 根据商品 ID 查询商品规格
     */
    @RequestMapping("/selectTbItemParamItemByItemId")
    public TbItemParamItem selectTbItemParamItemByItemId(@RequestParam Long itemId){
        return itemParamService.selectTbItemParamItemByItemId(itemId);
    }
    /**查询商品信息
     * 根据商品id
     * @param itemId
     * @return
     */
    @RequestMapping("/selectItemInfo")
    public TbItem selectItemInfo(Long itemId){
        return this.itemParamService.selectItemInfo(itemId);
    }

    /**
     * 根据商品 ID 查询商品描述
     */
    @RequestMapping("/selectItemDescByItemId")
    public TbItemDesc selectItemDescByItemId(Long itemId){
        return this.itemParamService.selectItemDescByItemId(itemId);
    }


    }
