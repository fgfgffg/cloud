package com.usian.controller;

import com.usian.feigin.FeignInter;
import com.usian.utils.Result;
import com.usian.pojo.TbItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/backend/itemCategory")
public class ItemCateGoryControllers {

    @Autowired
    private FeignInter feignInter;

    /**
     * 根据类目id查询当前类目的字节点
     * @param id
     * @return
     */
    @RequestMapping("/selectItemCategoryByParentId")
    public Result selectItemCategoryByParentId(@RequestParam(defaultValue ="0") Long id ){
         List<TbItemCat> list=feignInter.selectItemCategoryByParentId(id);
             if (list.size()>0){
             return Result.ok(list);
         }
         return Result.error("查无结果");
    }
}
