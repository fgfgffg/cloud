package com.usian.controller;

import com.usian.feigin.FeignInter;
import com.usian.pojo.TbItem;
import com.usian.utils.PageResult;
import com.usian.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
@RestController
@RequestMapping("/backend/item")
public class ContentController {

    @Autowired
   FeignInter feign;


    /**
     * 分页查询
     * @param page

     * @return
     */
    @RequestMapping("/selectTbItemAllByPage")
    public Result selectTbItemAllByPage(@RequestParam(defaultValue = "1") Integer page){
         PageResult pageResult=feign.selectTbItemAllByPage(page);
         if (pageResult.getResult()!=null && pageResult.getResult().size()>0){
             return Result.ok(pageResult);
         }
         return Result.error("查无结果");
    }

    /**
     * 添加商品
     */
    @GetMapping("/insertTbItem")
    public Result insertTbItem(TbItem tbItem,String desc,String itemParams){
        Integer insertTbItemNum = feign.insertTbItem(tbItem, desc,itemParams);
        if(insertTbItemNum==3){
            return Result.ok();
        }
        return Result.error("添加失败");
    }
  @RequestMapping("/deleteItemById")
    public Result deleteItemById(@RequestParam("itemId") Long itemId){
         Integer insertTbItem=feign.deleteItemById(itemId);
         if (insertTbItem>0){
             return Result.ok();
         }
         return Result.error("删除失败");
    }
 @PostMapping("/preUpdateItem")
    public Result preUpdateItem(@RequestParam Long itemId){
     Map<String,Object> map=feign.preUpdateItem(itemId);
     if(map.size()>0){
         return Result.ok(map);
     }
     return Result.error("查无结果");
    }
 @GetMapping("/updateTbItem")
    public Result updateTbItem(TbItem tbItem, String desc, String itemParams){
     Integer insertTbItem=feign.updateTbItem(tbItem,desc,itemParams);
     if (insertTbItem==3){
         return Result.ok();
     }
     return Result.error("查无结果");
    }

}
