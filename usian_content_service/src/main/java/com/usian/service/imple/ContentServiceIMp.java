package com.usian.service.imple;

//import RedisClient;
import com.usian.client.RedisClient;
import com.usian.mapper.TbContentCategoryMapper;
import com.usian.pojo.TbContentCategory;
import com.usian.pojo.TbContentCategoryExample;
import com.usian.utils.AdNode;
import com.usian.utils.PageResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.usian.mapper.TbContentMapper;
import com.usian.pojo.TbContent;
import com.usian.pojo.TbContentExample;
import com.usian.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service

public class ContentServiceIMp implements ContentService {
    @Autowired
    private TbContentMapper tbContentMapper;
    @Autowired
    private TbContentCategoryMapper tbContentCategoryMapper;
//   @Autowired
  private RedisClient redisClient;
    @Value("${AD_WIDTHB}")
    private Integer AD_WIDTHB;
    @Value("${AD_CATEGORY_ID}")
    private Integer AD_CATEGORY_ID;
    @Value("${PROTAL_CATRESULT_KEY}")
   private String PROTAL_CATRESULT_KEY;
    @Value("${AD_HEIGHT}")
    private Integer AD_HEIGHT;

    @Value("${AD_WIDTH}")
    private Integer AD_WIDTH;
    @Value("${PORTAL_AD_KEY}")
    private String PORTAL_AD_KEY;

    @Value("${AD_HEIGHTB}")
    private Integer AD_HEIGHTB;
    /**
     * 根据分类查询内容
     */
    @Override
    public PageResult selectTbContentAllByCategoryId(Integer page, Integer rows,
                                                     Long categoryId) {
        PageHelper.startPage(page, rows);
        TbContentExample example = new TbContentExample();
        TbContentExample.Criteria criteria = example.createCriteria();
        criteria.andCategoryIdEqualTo(categoryId);
        List<TbContent> list = this.tbContentMapper.selectByExample(example);
        PageInfo<TbContent> pageInfo = new PageInfo<TbContent>(list);
        PageResult result = new PageResult();
        result.setPageIndex(pageInfo.getPageNum());
        result.setTotalPage((int)pageInfo.getTotal());
        result.setResult(pageInfo.getList());
        return result;
    }

    @Override
    public Integer insertTbContent(TbContent tbContent) {

            tbContent.setUpdated(new Date());
            tbContent.setCreated(new Date());
        int i = tbContentMapper.insertSelective(tbContent);

        redisClient.hdel(PROTAL_CATRESULT_KEY,AD_CATEGORY_ID.toString());

        return i;

    }
    /**
     * 删除分类下的内容
     * @param id
     * @return
     */
    @Override
    public Integer deleteContentByIds(Long id) {
        int i = this.tbContentMapper.deleteByPrimaryKey(id);
      redisClient.hdel(PROTAL_CATRESULT_KEY,AD_CATEGORY_ID.toString());

        return i;
    }

    @Override
    public List<TbContentCategory> selectContentCategoryByParentId(Long id) {
        TbContentCategoryExample tbContentCategoryExample = new TbContentCategoryExample();
        TbContentCategoryExample.Criteria criteria = tbContentCategoryExample.createCriteria();
        criteria.andParentIdEqualTo(id);
        List<TbContentCategory> list =
                this.tbContentCategoryMapper.selectByExample(tbContentCategoryExample);
        return list;
    }
    @Override
    public List<AdNode> selectFrontendContentByAD() {
        //查询缓存
        List<AdNode> adNodeListRedis =
                (List<AdNode>)redisClient.hget(PROTAL_CATRESULT_KEY,AD_CATEGORY_ID.toString());
        if(adNodeListRedis!=null){
            return adNodeListRedis;
        }
        // 查询TbContent
        TbContentExample tbContentExample = new TbContentExample();
        TbContentExample.Criteria criteria = tbContentExample.createCriteria();
        criteria.andCategoryIdEqualTo((long) AD_CATEGORY_ID);
        List<TbContent> tbContentList =
                tbContentMapper.selectByExample(tbContentExample);
        List<AdNode> adNodeList = new ArrayList<AdNode>();
        for (TbContent tbContent : tbContentList) {
            AdNode adNode = new AdNode();
            adNode.setSrc(tbContent.getPic());
            adNode.setSrcB(tbContent.getPic2());
            adNode.setHref(tbContent.getUrl());
            adNode.setHeight(AD_HEIGHT);
            adNode.setWidth(AD_WIDTH);
            adNode.setHeightB(AD_HEIGHTB);
            adNode.setWidthB(AD_WIDTHB);
            adNodeList.add(adNode);
        }
        redisClient.hset(PORTAL_AD_KEY,AD_CATEGORY_ID.toString(),adNodeList);
        return adNodeList;
    }


}
