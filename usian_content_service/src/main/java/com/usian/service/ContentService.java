package com.usian.service;

import com.usian.pojo.TbContentCategory;
import com.usian.utils.AdNode;
import com.usian.utils.PageResult;
import com.usian.pojo.TbContent;

import java.util.List;

public interface ContentService {
    PageResult selectTbContentAllByCategoryId(Integer page, Integer rows, Long categoryId);

    Integer insertTbContent(TbContent tbContent);

    Integer deleteContentByIds(Long id);

    List<TbContentCategory> selectContentCategoryByParentId(Long id);

    List<AdNode> selectFrontendContentByAD();
}
