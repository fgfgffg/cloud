package com.usian.service;

import com.usian.pojo.TbContentCategory;
import com.usian.utils.AdNode;

import java.util.List;

public interface ContentCateSerivce {
    List<TbContentCategory> selectContentCategoryByParentId(Long parentId);



    Integer insertContentCategory(TbContentCategory tbContentCategory);

    Integer deleteContentCategoryById(Long categoryId);

    Integer updateContentCategory(TbContentCategory tbContentCategory);
}
