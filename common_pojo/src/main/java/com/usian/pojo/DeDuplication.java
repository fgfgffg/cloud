package com.usian.pojo;

import java.io.Serializable;
import java.util.Date;

public class DeDuplication implements Serializable {
    private String txNo;

    private Date createTime;

    private Integer state;

    public String getTxNo() {
        return txNo;
    }

    public void setTxNo(String txNo) {
        this.txNo = txNo == null ? null : txNo.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}