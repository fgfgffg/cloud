package com.usian.controller;

import com.usian.pojo.SearchItem;
import com.usian.service.SearchItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class SearchItemController {
    @Autowired
    private SearchItemService searchItemService;

    @RequestMapping("/importAll")
    public boolean importAll(){
        return searchItemService.importAll();
    }
    @RequestMapping("/list")
    public   List<SearchItem> selectByq(@RequestParam String q, @RequestParam Long page,
                               @RequestParam Integer pageSize){
        return searchItemService.selectByq(q,page,pageSize);
    }
}
