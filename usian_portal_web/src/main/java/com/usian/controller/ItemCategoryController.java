package com.usian.controller;

import com.usian.feigin.FeignInter;
import com.usian.utils.Result;

import com.usian.utils.CatResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页商品分类
 */
@RestController
@RequestMapping("/frontend/itemCategory/")
public class ItemCategoryController {
   @Autowired
    private FeignInter itemCateFeign;
//
//    /**
//     * 查询首页商品分类
//     */
    @RequestMapping(value = "selectItemCategoryAll",method = RequestMethod.GET)
    public Result selectItemCategoryAll() {
        CatResult catResult = itemCateFeign.selectItemCategoryAll();
        if(catResult.getData().size()>0){
            return Result.ok(catResult);
        }
        return Result.error("查无结果");
    }

}