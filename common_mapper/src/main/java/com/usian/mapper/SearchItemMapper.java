package com.usian.mapper;

import com.usian.pojo.SearchItem;

import java.util.List;

public interface SearchItemMapper {
    List<SearchItem> getItemList();

    Long findCount();
    SearchItem getItemById(Long itemId);
}
